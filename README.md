![Hero Image](./etc/img/hero.jpg)
(Picture Source / Credits: https://pixabay.com/users/derfotoartist-1882880/)

# drupal4aku

The [official Drupal Docker](https://github.com/docker-library/drupal/blob/master/8.7/fpm-alpine/Dockerfile) simple prepared to develop.

## Quick start
* Go to `./docker/drupal4aku/` 
* Start `docker-compose up`  
* Find the sources with in `./docker/drupal4aku/src`

## License
[License file](./LICENSE)